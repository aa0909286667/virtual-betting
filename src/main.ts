import { createApp } from 'vue'
import '@/styles/_reset.css'
import App from './App.vue'

createApp(App).mount('#app')
