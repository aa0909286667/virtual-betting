interface AnimationObj {
    duration: (duration: number) => AnimationObj;
    hideStart: () => AnimationObj;
}

export function moveTransition(startElementId: string, endElementId: string): AnimationObj {
    const startElement = document.getElementById(startElementId);
    const endElement = document.getElementById(endElementId);

    if (!startElement || !endElement) {
        return {
            duration: () => animationObj,
            hideStart: () => animationObj,
        };
    }

    startElement.style.visibility = 'visible';

    const clonedElement = cloneElement(startElement);
    const container = insertClonedElementToContainer(clonedElement, startElement);

    const animationObj: AnimationObj = {
        duration: (duration: number) => {
            animateElement(container, endElement, duration);
            return animationObj;
        },
        hideStart: () => {
            startElement.style.visibility = 'hidden';
            return animationObj;
        },
    };

    animateElement(container, endElement, 1000);
    return animationObj;
}

function cloneElement(element: HTMLElement): HTMLElement {
    const clonedElement = element.cloneNode(true) as HTMLElement;
    return clonedElement;
}

function insertClonedElementToContainer(clonedElement: HTMLElement, startElement: HTMLElement): HTMLElement {
    const container = document.createElement('div');
    container.style.position = 'absolute';
    container.style.top = `${startElement.getBoundingClientRect().top}px`;
    container.style.left = `${startElement.getBoundingClientRect().left}px`;
    clonedElement.style.pointerEvents = 'none';

    container.appendChild(clonedElement);
    document.body.appendChild(container);
    return container;
}

function animateElement(container: HTMLElement, endElement: HTMLElement, duration: number): void {
    const endRect = endElement.getBoundingClientRect();
    const containerRect = container.getBoundingClientRect();

    const offsetX = (endRect.width - containerRect.width) / 2;
    const offsetY = (endRect.height - containerRect.height) / 2;

    const animation = container.animate(
        [
            { transform: 'translate3D(0, 0, 0)' },
            { transform: `translate3D(${endRect.left - containerRect.left + offsetX}px, ${endRect.top - containerRect.top + offsetY}px, 0)` },
        ],
        {
            duration,
            fill: 'forwards',
            iterations: 1,
        }
    );

    animation.onfinish = () => {
        if (container.parentElement) {
            document.body.removeChild(container);
        }
    };
}



