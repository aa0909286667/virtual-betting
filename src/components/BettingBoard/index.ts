interface BettingBoardProps {
    numbers: string[] | number[];
    price?: PriceList[] | null;
    size?: {
        width: number;
        height: number;
    };
    colors?: {
        primary?: string;
        secondary?: string;
        middle?: string;
        index?: (index: number) => string;
    };
    borderColor?: string;
    borderWidth?: number;
    backgroundColor?: string;
    fontColor?: string;
    fontSize?: number;
    fontWeight?: 'normal' | 'bold' | 'bolder' | 'lighter';
    textAlign?: 'left' | 'center' | 'right';
    padding?: number | string;
    margin?: number | string;
}

interface PriceList {
    number: string | number,
    priceList: number[]
}

// 圓形下注盤的屬性接口
interface CircleBettingBoardProps extends BettingBoardProps {
    shape: 'circle';
    circleType: 'roulette' | 'luckyWheel' | 'lotteryGame' | 'guessHighLow' | 'custom';
    circleOptions?: {
        [key: string]: any;
    };
}

// 扇形下注盤的屬性接口
interface FanBettingBoardProps extends BettingBoardProps {
    shape: 'fan';
    fanType: 'FanShapedLotteryGame';
    fanOptions?: {
        [key: string]: any;
    };
}

// 梯形下注盤的屬性接口
interface TrapezoidBettingBoardProps extends BettingBoardProps {
    shape: 'trapezoid';
    trapezoidType: 'TrapezoidBetGame';
    trapezoidOptions?: {
        [key: string]: any;
    };
}

// 長方形下注盤的屬性接口
interface RectangleBettingBoardProps extends BettingBoardProps {
    shape: 'rectangle';
    rectangleType: 'Baccarat' | 'SimilarGame';
    rectangleOptions?: {
        [key: string]: any;
    };
}

// 正方形下注盤的屬性接口
interface SquareBettingBoardProps extends BettingBoardProps {
    shape: 'square';
    squareType: 'Baccarat' | 'SimilarGame';
    squareOptions?: {
        [key: string]: any;
    };
}

// 橢圓形下注盤的屬性接口
interface EllipseBettingBoardProps extends BettingBoardProps {
    shape: 'ellipse';
    ellipseType: 'EllipseBetGame';
    ellipseOptions?: {
        [key: string]: any;
    };
}

type BettingBoardPropsUnion =
    CircleBettingBoardProps |
    FanBettingBoardProps |
    TrapezoidBettingBoardProps |
    RectangleBettingBoardProps |
    SquareBettingBoardProps |
    EllipseBettingBoardProps;

export default BettingBoardPropsUnion;
